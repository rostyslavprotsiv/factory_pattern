package com.factorypattern.controller;

import com.factorypattern.model.action.factories.Bakery;
import com.factorypattern.model.action.factories.BakeryDnipro;
import com.factorypattern.model.action.factories.BakeryKyiv;
import com.factorypattern.model.action.factories.BakeryLviv;
import com.factorypattern.model.action.pizzaactions.PizzaAction;
import com.factorypattern.model.entity.condition.Condition;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public final class Controller {
    private final Logger LOGGER = LogManager.getLogger(Controller.class);
    private final String[] bakeries = {"Dnipro", "Lviv", "Kyiv"};

    public String getPizza(String type, String bakery) {
        Condition condition = getCondition(type);
        if (condition == null) {
            String logMessage = "Such condition doesn't exist";
            LOGGER.info(logMessage);
            LOGGER.warn(logMessage);
            LOGGER.error(logMessage);
            return null;
        }
        Bakery bakeryFactory = getBakery(bakery);
        if (bakeryFactory == null) {
            String logMessage = "Such bakery doesn't exist";
            LOGGER.info(logMessage);
            LOGGER.warn(logMessage);
            LOGGER.error(logMessage);
            return null;
        }
        PizzaAction action = bakeryFactory.getPizzaLogicInstance(condition);
        if (action == null) {
            return null;
        }
        return action.getPizza().toString();
    }

    private Condition getCondition(String type) {
        Condition condition = null;
        Condition[] values = Condition.values();
        for (int i = 0; i < values.length; i++) {
            if (values[i].name().equals(type.toUpperCase())) {
                condition = values[i];
                break;
            }
        }
        return condition;
    }

    private Bakery getBakery(String bakery) {
        Bakery bakeryFactory = null;
        if (bakery.equals(bakeries[0])) {
            bakeryFactory = new BakeryDnipro();
        } else if (bakery.equals(bakeries[1])) {
            bakeryFactory = new BakeryLviv();
        } else if (bakery.equals(bakeries[2])) {
            bakeryFactory = new BakeryKyiv();
        }
        return bakeryFactory;
    }
}
