package com.factorypattern.model.action.factories;

import com.factorypattern.model.action.pizzaactions.PizzaAction;
import com.factorypattern.model.action.pizzaactions.impl.*;
import com.factorypattern.model.entity.condition.Condition;

public class BakeryKyiv extends Bakery {
    @Override
    protected PizzaAction getPizzaLogic(Condition condition) {
        PizzaAction pizza = null;
        if (condition == condition.PEPPERONI) {
            return new PepperoniPizzaAction();
        }
        if (condition == condition.VEGIE) {
            return new VeggiePizzaAction();
        }
        if (condition == condition.SEAFOOD) {
            return new SeafoodPizzaAction();
        }
        if (condition == condition.CHEESE) {
            return new CheesePizzaAction();
        }
        if (condition == condition.MARGARITA) {
            return new MargaritaPizzaAction();
        }
        return pizza;
    }
}
