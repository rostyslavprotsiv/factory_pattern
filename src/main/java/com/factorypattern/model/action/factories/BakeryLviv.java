package com.factorypattern.model.action.factories;

import com.factorypattern.model.action.pizzaactions.PizzaAction;
import com.factorypattern.model.action.pizzaactions.impl.MargaritaPizzaAction;
import com.factorypattern.model.action.pizzaactions.impl.PepperoniPizzaAction;
import com.factorypattern.model.action.pizzaactions.impl.VeggiePizzaAction;
import com.factorypattern.model.entity.condition.Condition;

public class BakeryLviv extends Bakery {
    @Override
    protected PizzaAction getPizzaLogic(Condition condition) {
        PizzaAction pizza = null;
        if (condition == condition.PEPPERONI) {
            return new PepperoniPizzaAction();
        }
        if (condition == condition.VEGIE) {
            return new VeggiePizzaAction();
        }
        if (condition == condition.SEAFOOD) {
            LOGGER.info("Sorry, but seafood pizza is missing in Lviv.");
        }
        if (condition == condition.CHEESE) {
            LOGGER.info("Sorry, but cheeze pizza is missing in Lviv.");
        }
        if (condition == condition.MARGARITA) {
            return new MargaritaPizzaAction();
        }
        return pizza;
    }
}
