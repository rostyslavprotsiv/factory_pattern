package com.factorypattern.model.action.pizzaactions;

import com.factorypattern.model.entity.pizza.pizzaentity.Pizza;

public interface PizzaAction {

    void prepare();

    void bake();

    void cut();

    void box();

    Pizza getPizza();
}
