package com.factorypattern.model.action.pizzaactions.impl;

import com.factorypattern.model.action.pizzaactions.PizzaAction;
import com.factorypattern.model.entity.pizza.pizzaentity.CheesePizza;
import com.factorypattern.model.entity.pizza.pizzaentity.Pizza;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class CheesePizzaAction implements PizzaAction {
    private final Logger LOGGER = LogManager.getLogger(CheesePizzaAction.class);

    @Override
    public void prepare() {
        LOGGER.info(getPizza().getType() + " pizza with "
                + getPizza().getSauce() + " sauce and "
                + getPizza().getToppings() + " toppings is prepared.");
    }

    @Override
    public void bake() {
        LOGGER.info(getPizza().getType() + " pizza with "
                + getPizza().getDough() + " dough is baked.");
    }

    @Override
    public void cut() {
        LOGGER.info(getPizza().getType() + " pizza is cut.");
    }

    @Override
    public void box() {
        LOGGER.info(getPizza().getType() + " pizza is boxed.");
    }

    @Override
    public Pizza getPizza() {
        return new CheesePizza();
    }
}
