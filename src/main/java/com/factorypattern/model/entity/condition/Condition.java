package com.factorypattern.model.entity.condition;

/**
 * @Author Ivan Ostapchuk
 * @since 21.05.19
 */

public enum Condition {
    CHEESE, MARGARITA, PEPPERONI, SEAFOOD, VEGIE
}

