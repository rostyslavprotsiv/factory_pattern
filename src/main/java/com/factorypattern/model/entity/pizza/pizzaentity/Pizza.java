package com.factorypattern.model.entity.pizza.pizzaentity;


import com.factorypattern.model.entity.pizza.pizzaenum.Dough;
import com.factorypattern.model.entity.pizza.pizzaenum.Sauce;
import com.factorypattern.model.entity.pizza.pizzaenum.Toppings;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public abstract class Pizza {
    private String type;
    private Dough dough;
    private Sauce sauce;
    private List<Toppings> toppings;

    public Pizza() {
    }

    public Pizza(String type, Dough dough, Sauce sauce,
                 ArrayList<Toppings> toppings) {
        this.type = type;
        this.dough = dough;
        this.sauce = sauce;
        this.toppings = toppings;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Dough getDough() {
        return dough;
    }

    public void setDough(Dough dough) {
        this.dough = dough;
    }

    public Sauce getSauce() {
        return sauce;
    }

    public void setSauce(Sauce sauce) {
        this.sauce = sauce;
    }

    public List<Toppings> getToppings() {
        return toppings;
    }

    public void setToppings(List<Toppings> toppings) {
        this.toppings = toppings;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pizza pizza = (Pizza) o;
        return Objects.equals(type, pizza.type) &&
                dough == pizza.dough &&
                sauce == pizza.sauce &&
                Objects.equals(toppings, pizza.toppings);
    }

    @Override
    public int hashCode() {
        return Objects.hash(type, dough, sauce, toppings);
    }

    @Override
    public String toString() {
        return "Pizza{" +
                "type='" + type + '\'' +
                ", dough=" + dough +
                ", sauce=" + sauce +
                ", toppings=" + toppings +
                '}';
    }
}
