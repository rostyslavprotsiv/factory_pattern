package com.factorypattern.model.entity.pizza.pizzaentity;

import com.factorypattern.model.entity.pizza.pizzaenum.Dough;
import com.factorypattern.model.entity.pizza.pizzaenum.Sauce;
import com.factorypattern.model.entity.pizza.pizzaenum.Toppings;

import java.util.ArrayList;
import java.util.List;

public class SeafoodPizza extends Pizza {
    public SeafoodPizza() {
        super("Seafood", Dough.THIN, Sauce.WHITE, new ArrayList<Toppings>());
        this.setToppings(getStandardToppings());
    }

    public SeafoodPizza(String type, Dough dough, Sauce sauce,
                        ArrayList<Toppings> toppings) {
        super(type, dough, sauce, toppings);
    }

    private List<Toppings> getStandardToppings() {
        List<Toppings> toppings = new ArrayList<>();
        toppings.add(Toppings.SALMON);
        toppings.add(Toppings.SHRIMS);
        toppings.add(Toppings.TUNA);
        toppings.add(Toppings.MOZZARELLA);
        toppings.add(Toppings.ARUGULA);
        toppings.add(Toppings.OLIVE);
        toppings.add(Toppings.ONION);
        return toppings;
    }
}
