package com.factorypattern.model.entity.pizza.pizzaentity;

import com.factorypattern.model.entity.pizza.pizzaenum.Dough;
import com.factorypattern.model.entity.pizza.pizzaenum.Sauce;
import com.factorypattern.model.entity.pizza.pizzaenum.Toppings;

import java.util.ArrayList;
import java.util.List;

public class VegiePizza extends Pizza {
    public VegiePizza() {
        super("Vegie", Dough.THIN, Sauce.CAESAR, new ArrayList<Toppings>());
        this.setToppings(getStandardToppings());
    }

    public VegiePizza(String type, Dough dough, Sauce sauce,
                      ArrayList<Toppings> toppings) {
        super(type, dough, sauce, toppings);
    }

    private List<Toppings> getStandardToppings() {
        List<Toppings> toppings = new ArrayList<>();
        toppings.add(Toppings.MUSHROOMS);
        toppings.add(Toppings.TOMATO);
        toppings.add(Toppings.SALAD);
        toppings.add(Toppings.BASIL);
        toppings.add(Toppings.OLIVE);
        toppings.add(Toppings.CABBAGE);
        toppings.add(Toppings.ONION);
        return toppings;
    }
}
