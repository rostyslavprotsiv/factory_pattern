package com.factorypattern.model.entity.pizza.pizzaenum;

public enum Dough {
  THIN, THICK
}
