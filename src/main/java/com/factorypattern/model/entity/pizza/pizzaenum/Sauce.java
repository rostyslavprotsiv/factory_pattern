package com.factorypattern.model.entity.pizza.pizzaenum;

public enum Sauce {
    WHITE, TOMATO, MARINARA, PESTO, CAESAR, BBQ
}
