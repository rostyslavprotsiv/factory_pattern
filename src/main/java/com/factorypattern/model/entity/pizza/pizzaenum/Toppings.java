package com.factorypattern.model.entity.pizza.pizzaenum;

public enum Toppings {
    //  Meet
    CHICKEN, HAM, BACON, PROSCIUTTO, SALAMI, PEPPERONI,
    //  Seafood
    SALMON, SHRIMS, TUNA,
    //  Cheese
    PARMESAN, MOZZARELLA, CHEDDAR, DORBLU,
    //  All the rest of it
    TOMATO, CORN, OLIVE, EGG, CHILE, ONION, BASIL, MUSHROOMS, SALAD,
    ARUGULA, CABBAGE, ZUCCHINI
}
