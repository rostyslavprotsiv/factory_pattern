package com.rostyslavprotsiv.model.action.pizzaactions.impl;

import com.factorypattern.model.action.pizzaactions.impl.CheesePizzaAction;
import com.factorypattern.model.entity.pizza.pizzaentity.CheesePizza;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CheesePizzaActionTest {
    @Test
    public void testGetPizza() {
        CheesePizza pizza = new CheesePizza();
        CheesePizzaAction action = new CheesePizzaAction();
        assertEquals(pizza, action.getPizza());
    }
}
